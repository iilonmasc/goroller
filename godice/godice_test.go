package godice

import (
	"fmt"
	"testing"

	"gitlab.com/iilonmasc/sambalib/v3/logutil"
	"gitlab.com/iilonmasc/sambalib/v3/sysutil"
)

type Condition struct {
	Min        int
	Max        int
	Exceptions []int
}

func TestParsing(t *testing.T) {
	fn := "[ " + sysutil.FuncName() + " ]:"
	testLogger := &logutil.Logger{}
	testLogger.SetOptions(logutil.LogLevel(logutil.Critical), logutil.Prefix("testing"))
	cases := []struct {
		description string
		in          string
		want        Notation
	}{
		{"notation without drop instruction", "1d6", Notation{Amount: 1, Type: 6}},
		{"notation including spaces before dice without drop instruction", "1 d6", Notation{Amount: 1, Type: 6}},
		{"notation including spaces after separator without drop instruction", "1d 6", Notation{Amount: 1, Type: 6}},
		{"notation including spaces before and after separator without drop instruction", "1 d 6", Notation{Amount: 1, Type: 6}},
		{"notation with drop instruction", "8d12>4", Notation{Amount: 8, Type: 12, DropBelow: 4, Modifier: 0}},
		{"notation including spaces before separator with drop instruction", "8 d12>4", Notation{Amount: 8, Type: 12, DropBelow: 4}},
		{"notation including spaces after separator with drop instruction", "8d 12>4", Notation{Amount: 8, Type: 12, DropBelow: 4}},
		{"notation including spaces before and after separator with drop instruction", "8 d 12>4", Notation{Amount: 8, Type: 12, DropBelow: 4}},
		{"notation including spaces before drop with drop instruction", "8d12 >4", Notation{Amount: 8, Type: 12, DropBelow: 4}},
		{"notation including spaces after drop with drop instruction", "8d12> 4", Notation{Amount: 8, Type: 12, DropBelow: 4}},
		{"notation including spaces before and after drop with drop instruction", "8d12 > 4", Notation{Amount: 8, Type: 12, DropBelow: 4}},
		{"notation including spaces everywhere with drop instruction", "8 d 12 > 4", Notation{Amount: 8, Type: 12, DropBelow: 4}},
		{"notation including poositive modifier ", "1d6+10", Notation{Amount: 1, Type: 6, Modifier: 10}},
		{"notation including negative modifier ", "1d6-10", Notation{Amount: 1, Type: 6, Modifier: -10}},
		{"notation for success roll", "8d12>s4", Notation{Amount: 8, Type: 12, DropBelow: 4, SuccessRoll: true}},
	}
	for _, c := range cases {
		got := ParseNotation(c.in)
		if got != c.want {
			t.Errorf("%q %q godice.ParseNotation(%q) == {Amount: %d, Type: %d, DropBelow: %d}, want {Amount: %d, Type: %d, DropBelow: %d}", fn, c.description, c.in, got.Amount, got.Type, got.DropBelow, c.want.Amount, c.want.Type, c.want.DropBelow)
		} else {
			fmt.Println(fn, c.description, ": ✓")
		}
	}
}

func TestRolling(t *testing.T) {
	fn := "[ " + sysutil.FuncName() + " ]:"
	testLogger := &logutil.Logger{}
	testLogger.SetOptions(logutil.LogLevel(logutil.Critical), logutil.Prefix("testing"))
	cases := []struct {
		description string
		in          Notation
		want        Condition
	}{
		{"roll testing 1d6+6", Notation{Amount: 1, Type: 6, Modifier: 6}, Condition{Min: 7, Max: 12, Exceptions: []int{0}}},
		{"roll testing 1d6-6", Notation{Amount: 1, Type: 6, Modifier: -6}, Condition{Min: -5, Max: 0, Exceptions: []int{0}}},
		{"roll testing 1d6", Notation{Amount: 1, Type: 6}, Condition{Min: 1, Max: 6, Exceptions: []int{0}}},
		{"roll testing 1d6", Notation{Amount: 1, Type: 6}, Condition{Min: 1, Max: 6}},
		{"roll testing 2d6>4", Notation{Amount: 2, Type: 6, DropBelow: 4}, Condition{Min: 8, Max: 12, Exceptions: []int{0, 4, 5, 6}}},
		{"roll testing 2d6>s4", Notation{Amount: 2, Type: 6, DropBelow: 4, SuccessRoll: true}, Condition{Min: 0, Max: 2}},
	}
	for _, c := range cases {
		foundException := false
		valid := false
		got := c.in.Roll().Sum
		if got <= c.want.Max && got >= c.want.Min {
			fmt.Println(fn, c.description, ": ✓")
			valid = true
		} else {
			// fmt.Println(c.description, got)
			if len(c.want.Exceptions) != 0 {
				for _, i := range c.want.Exceptions {
					if got == i {
						fmt.Println(fn, c.description, ": ✓")
						foundException = true
					}
				}
			}
		}
		if !valid && !foundException {
			t.Errorf("%q %q godice.Roll %+v == %+v, want %+v", fn, c.description, c.in, got, c.want)
		} /*else if !valid && foundException {
		fmt.Println(c.description, got, "Exception", foundException)
		  // fmt.Println(fn, c.description, ": ✓")
		}*/
	}
}
