package godice

import (
	"math/rand"
	"strconv"
	"strings"
	"time"
)

type Notation struct {
	Amount      int
	Type        int
	DropBelow   int
	Modifier    int
	SuccessRoll bool
}
type Roll struct {
	Rolled      []int
	Modifier    int
	Sum         int
	SuccessRoll bool
}

func ParseNotation(notation string) Notation {
	notation = strings.ReplaceAll(notation, " ", "")
	diceAmount, diceType, dropAmount, flatModifier := 0, 0, 0, 0
	SuccessRoll := false
	modifierSplit := []string{}

	if strings.Contains(notation, "+") {
		modifierSplit = strings.Split(notation, "+")
		flatModifier, _ = strconv.Atoi(modifierSplit[1])
		notation = modifierSplit[0]

	} else if strings.Contains(notation, "-") {
		modifierSplit = strings.Split(notation, "-")
		flatModifier, _ = strconv.Atoi("-" + modifierSplit[1])
		notation = modifierSplit[0]
	} else {
		flatModifier = 0
	}

	notationSplit := strings.Split(notation, "d")
	diceAmount, _ = strconv.Atoi(notationSplit[0])
	if strings.Contains(notationSplit[1], ">") {
		dropBelowSplit := strings.Split(notationSplit[1], ">")
		diceType, _ = strconv.Atoi(dropBelowSplit[0])
		if strings.Contains(dropBelowSplit[1], "s") {
			successSplit := strings.Split(dropBelowSplit[1], "s")
			dropAmount, _ = strconv.Atoi(successSplit[1])
			SuccessRoll = true
		} else {
			dropAmount, _ = strconv.Atoi(dropBelowSplit[1])
			SuccessRoll = false
		}
	} else {
		diceType, _ = strconv.Atoi(notationSplit[1])
		dropAmount = 0
	}
	return Notation{Amount: diceAmount, Type: diceType, DropBelow: dropAmount, Modifier: flatModifier, SuccessRoll: SuccessRoll}
}

func (notation *Notation) Roll() Roll {
	resultRoll := Roll{Modifier: notation.Modifier}
	if notation.SuccessRoll {
		resultRoll.SuccessRoll = true
	}
	for i := 1; i <= notation.Amount; i++ {
		rand.Seed(time.Now().UnixNano())
		roll := rand.Intn(notation.Type) + 1
		if roll >= notation.DropBelow && !notation.SuccessRoll {
			resultRoll.Rolled = append(resultRoll.Rolled, roll)
		} else if roll >= notation.DropBelow {
			resultRoll.Rolled = append(resultRoll.Rolled, roll)
		}
	}
	if resultRoll.SuccessRoll {
		for range resultRoll.Rolled {
			resultRoll.Sum += 1
		}
	} else {
		for _, roll := range resultRoll.Rolled {
			resultRoll.Sum += roll
		}
	}
	resultRoll.Sum += resultRoll.Modifier
	return resultRoll
}
