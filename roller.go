package main

import (
	"flag"
	"os"

	"gitlab.com/iilonmasc/goroller/godice"
)

func initialize() ([]string, chan LogMessage) {

	var verboseFlag = flag.Bool("v", false, "Enable verbose output")
	var debugFlag = flag.Bool("d", false, "Enable debug output")
	logLevel := OUTPUT

	flag.Parse()
	args := flag.Args()
	logger := make(chan LogMessage)
	if *debugFlag {
		logLevel = DEBUG
	} else if *verboseFlag {
		logLevel = INFO
	}
	go log(logLevel, logger)
	logger <- buildMessage(DEBUG, "args", args)
	if len(args) >= 1 {
		return args, logger
	} else {
		logger <- buildMessage(WARN, "No args")
		return []string{}, logger
	}
}

func main() {
	args, logger := initialize()
	if len(args) == 0 {
		os.Exit(1)
	}
	var notation godice.Notation
	for i, roll := range args {
		logger <- buildMessage(INFO, i+1, "-->", roll)
		notation = godice.ParseNotation(roll)
		result := notation.Roll()
		logger <- buildMessage(INFO, "rolled", result.Rolled, "modifier", result.Modifier)
		if result.SuccessRoll {
			logger <- buildMessage(OUTPUT, result.Sum, "successes")
		} else {
			logger <- buildMessage(OUTPUT, result.Sum)
		}
	}
	logger <- buildMessage(DEBUG, "done, closing logger")
	close(logger)
}
