package main

type verbosity int

const (
	// Default logging is Low (zero-value of an int).
	ERROR = verbosity(iota)
	WARN
	OUTPUT
	INFO
	DEBUG
)

type LogMessage struct {
	Level   verbosity
	Message string
}
