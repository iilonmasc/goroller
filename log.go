package main

import "fmt"

const (
	colorReset = "\033[0m"

	colorRed    = "\033[31m"
	colorGreen  = "\033[32m"
	colorYellow = "\033[33m"
	colorCyan   = "\033[36m"
)

func log(logLevel verbosity, logChannel chan LogMessage) {
	for message := range logChannel {
		if message.Level <= logLevel {
			fmt.Println(message.Message)
		}
	}
}
func buildMessage(level verbosity, message ...interface{}) LogMessage {
	outputMessage := ""
	switch level {
	case DEBUG:
		outputMessage += colorCyan + "[DBG"
	case INFO:
		outputMessage += colorReset + "[INF"
	case WARN:
		outputMessage += colorYellow + "[WRN"
	case ERROR:
		outputMessage += colorRed + "[ERR"
	case OUTPUT:
		outputMessage += colorGreen + "> "
	}
	if level != OUTPUT {
		outputMessage += "]: "
	}
	for _, m := range message {
		outputMessage += fmt.Sprint(m) + " "
	}
	outputMessage += colorReset
	logMessage := LogMessage{Level: level, Message: outputMessage}
	return logMessage
}
